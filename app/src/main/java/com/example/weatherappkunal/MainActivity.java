package com.example.weatherappkunal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import retrofit2.converter.gson.GsonConverterFactory;

interface requestApi
{
    public void getCurrentData();
}

public class MainActivity extends AppCompatActivity {
    public static String AppId = "f7e7f4c8fd5df46353a72a3a5b67771f";
    public static String lat = "39.8865";
    public static String q="";
    public static String lon = "-83.4483";
    private TextView weatherData;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<RecyclerList> items = new ArrayList<>();
    private TextView total;
    private String TAG = "MainActivity";
    private EditText cityname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityname=findViewById(R.id.xmlcityname);
        recyclerView = findViewById(R.id.rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new MyAdapter(items, this));


//        weatherData = findViewById(R.id.textView);


        findViewById(R.id.xmlbutton).setOnClickListener(v->{
                q=cityname.getText().toString();
                requestApi ra=()->{

                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("http://api.openweathermap.org/")
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        WeatherService service = retrofit.create(WeatherService.class);
                        Log.d(TAG,"!!! getcurrentdata");

                        Call<WeatherResponse> call = service.getCurrentWeatherData(q,AppId);
                        call.enqueue(new Callback<WeatherResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<WeatherResponse> call, @NonNull Response<WeatherResponse> response) {
                                if (response.code() == 200) {
                                    WeatherResponse weatherResponse = response.body();
                                    assert weatherResponse != null;
                                    Log.d(TAG,"!!!On response");
//                    String stringBuilder = "Country: " +
//                            weatherResponse.sys.country +
//                            "\n" +
//                            "Temperature: " +
//                            weatherResponse.main.temp +
//                            "\n" +
//                            "Temperature(Min): " +
//                            weatherResponse.main.temp_min +
//                            "\n" +
//                            "Temperature(Max): " +
//                            weatherResponse.main.temp_max +
//                            "\n" +
//                            "Humidity: " +
//                            weatherResponse.main.humidity +
//                            "\n" +
//                            "Pressure: " +
//                            weatherResponse.main.pressure+
//                            "\n"+
//                            "Date:  "+weatherResponse.weather.get(0).icon
//                            ;
                                    Log.d(TAG, "!!! DATA =" + weatherResponse.weather.get(0).main);
//                    String showlog;
//                    weatherResponse.main.setTemp(weatherResponse.main.temp);
//                    weatherResponse.sys.setCountry(weatherResponse.sys.country);
//                    showlog = weatherResponse.sys.getCountry();
//                    weatherData.setText(stringBuilder);
//

                                    RecyclerList recyclerList=new RecyclerList(weatherResponse.sys.country,String.valueOf((Math.round((weatherResponse.main.temp)/10))),weatherResponse.name,String.valueOf(weatherResponse.main.humidity),weatherResponse.weather.get(0).description,weatherResponse.weather.get(0).main,String.valueOf(weatherResponse.wind.speed),String.valueOf(weatherResponse.main.pressure));
                                    items.add(recyclerList);
                                    recyclerView.getAdapter().notifyItemInserted(items.size() - 1);
                                    recyclerView.smoothScrollToPosition(items.size()-1);
//                    Log.d(TAG, "!!! country =" + showlog);


                                }
                            }


                            @Override
                            public void onFailure(@NonNull Call<WeatherResponse> call, @NonNull Throwable t) {
                                weatherData.setText(t.getMessage());
                            }
                        });


                };
                ra.getCurrentData();
                Log.d(TAG,"!!! Button clicked");

        });

    }




}



