package com.example.weatherappkunal;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyAdapterViewHolder> {
    private String TAG=getClass().getSimpleName();
    private List<RecyclerList> list;
    private Context context;
    private WeatherResponse weatherResponse;

    public MyAdapter(List<RecyclerList> list, Context context)
    {
        this.list=list;
        this.context=context;
    }


    @NonNull
    @Override
    public MyAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.weather, parent, false);
        return new MyAdapterViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapterViewHolder holder, int position) {

        final RecyclerList recyclerList=list.get(position);
        Log.d(TAG,"!!! onbindview holder  city = "+recyclerList.main);
        holder.xmlcity.setText(" "+recyclerList.name);
        holder.xmlhumidity.setText(recyclerList.humidity+"%");
        holder.weatherCondition=recyclerList.main;


        if(recyclerList.description.equals("light rain")) {
                holder.weatherImage.setImageResource(R.drawable.ligthrain);
            }
        if(recyclerList.description.equals("scattered clouds")) {
            holder.weatherImage.setImageResource(R.drawable.scatteredcloud);
        }
        if(recyclerList.description.equals("sky is clear")) {
            holder.weatherImage.setImageResource(R.drawable.clearsky);
        }
        if(recyclerList.description.equals("heavy intensity rain")) {
            holder.weatherImage.setImageResource(R.drawable.heavyrain);
        }
        if(recyclerList.description.equals("few clouds")) {
            holder.weatherImage.setImageResource(R.drawable.fewclouds);
        }
        if(recyclerList.description.equals("heavy intensity rain")) {
            holder.weatherImage.setImageResource(R.drawable.heavyrain);
        }
        if(recyclerList.description.equals("moderate rain")) {
            holder.weatherImage.setImageResource(R.drawable.moderaterain);
        }
        if(recyclerList.description.equals("broken clouds")) {
            holder.weatherImage.setImageResource(R.drawable.brokenclouds);
        }if(recyclerList.description.equals("overcast clouds")) {
            holder.weatherImage.setImageResource(R.drawable.overcastclouds);
        }
        if(recyclerList.description.equals("light shower snow")) {
            holder.weatherImage.setImageResource(R.drawable.lightshowerrain);
        }
//
//            case "moderate rain":
//                holder.weatherImage.setImageResource(R.drawable.moderaterain);
//                break;
//
//                default:
//                    holder.weatherImage.setImageResource(R.drawable.clearsky);
//                    break;





        holder.xmldescription.setText(recyclerList.description);
        holder.xmlcountry.setText(","+recyclerList.country);
        holder.xmltemp.setText("  "+recyclerList.temp+"C");
        holder.xmlpressure.setText("  "+recyclerList.pressure+"pa");
        holder.xmlwind.setText("  "+recyclerList.wind+"m/s");

        String currentDate = new SimpleDateFormat(" EEE \n dd MMM", Locale.getDefault()).format(new Date());

        holder.xmldate.setText(currentDate);


        Log.d(TAG,"!!! onbindview holder  Success "+recyclerList.wind);




    }



    @Override
    public int getItemCount() {
        return list.size();
    }



    public class MyAdapterViewHolder extends RecyclerView.ViewHolder
    {

        TextView xmlcity,xmltemp,xmldescription,xmlcountry,xmldate,xmlhumidity,xmlwind,xmlpressure;
        ImageView weatherImage;
        String weatherCondition;
        public MyAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            xmlcity=itemView.findViewById(R.id.xml_city);
            xmltemp=itemView.findViewById(R.id.xml_temp);
            weatherImage=itemView.findViewById(R.id.xml_image);
            xmlcountry=itemView.findViewById(R.id.xml_country);
            xmldescription=itemView.findViewById(R.id.xml_description);
            xmldate=itemView.findViewById(R.id.xml_date);
            xmlhumidity=itemView.findViewById(R.id.xml_humidity);
            xmlwind=itemView.findViewById(R.id.xml_wind);
            xmlpressure=itemView.findViewById(R.id.xml_pressure);

        }
    }

}
